import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, OnChanges {

  @Input() total = 0;
  @Input() perPage = 0;
  @Input() currentPage = 0;
  @Output() pageChangeHandler = new EventEmitter<number>();
  @Output() perPageChangeHandler = new EventEmitter<number>();

  pageNumbers = [];

  pageHandler(page: number) {
    this.pageChangeHandler.emit(page);
  }

  perPageHandler(perPage: number) {
    this.perPageChangeHandler.emit(perPage);

    this.pageNumbers = [];
    for (let i = 1; i <= Math.ceil(this.total / perPage); i++) {
      this.pageNumbers.push(i);
    }
    this.pageHandler(1);
  }

  constructor() { }

  ngOnInit() {}

  ngOnChanges() {
    if (!this.pageNumbers.length) {
      for (let i = 1; i < Math.ceil(this.total / this.perPage); i++) {
        this.pageNumbers.push(i);
      }
    }
  }

}
