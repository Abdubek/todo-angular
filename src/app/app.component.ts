import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  users = [];

  total = 5;
  perPage = 5;
  currentPage = 1;

  pageChangeHandler(page: number) {
    this.currentPage = page;
  }

  perPageChangeHandler(perPage: number) {
    this.perPage = perPage;
  }

  ngOnInit(): void {

    fetch('https://5be40ffc95e4340013f88e79.mockapi.io/api/v1/users')
      .then(res => res.json())
      .then(res => {
        this.users = res;
        this.total = this.users.length;
      });

  }
}
